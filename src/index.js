import express from 'express';
import cors from 'cors';
import fetch from '../node_modules/isomorphic-fetch';

import mw3a from './mw/mw3a';

import task2a from './tasks/task2a';
import task2b from './tasks/task2b';
import task2c from './tasks/task2c';
import task3a from './tasks/task3a';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    skb: 'homework',
  });
});

app.get('/task2a', task2a);
app.get('/task2b', task2b);
app.get('/task2c', task2c);
app.get('/task3a/?*', mw3a, task3a);

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
