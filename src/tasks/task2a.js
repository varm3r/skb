export default function task2a(req, res) {
  const sum = (+req.query.a || 0) + (+req.query.b || 0);
  res.send(sum.toString());
}
