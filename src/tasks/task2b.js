export default function task2b(req, res) {
  const re = /^(\s*[^\s\d[\]\\!@#$%^&*()`~_=+|;:",.<>/?]+\s*){1,3}$/;
  const fullname = (req.query.fullname || '').trim();
  let data = [];
  let lastname;

  try {
    if (!fullname.match(re)) {
      throw new Error();
    }
    data = fullname
      .toLowerCase()
      .split(/\s+/)
      .map(v => v[0].toUpperCase() + v.substring(1));
    lastname = data.pop();
    data = data
      .map(v => `${v[0]}.`);
    data.unshift(lastname);
  } catch (e) {
    res.send('Invalid fullname');
    return;
  }

  res.send(data.join(' '));
}
