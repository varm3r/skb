export default function task2c(req, res) {
  const re = new RegExp('^(?:https?:)?(?://)?(?:[^/]+(?:[0-9]+)?/)?@?([a-z0-9._-]+)', 'i');
  let username = req.query.username;
  let data;
  try {
    data = username.match(re);
    if (!data) {
      throw new Error();
    }
    username = `@${data[1]}`;
  } catch (e) {
    res.send('Invalid fullname');
    return;
  }
  res.send(username);
}
