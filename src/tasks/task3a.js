export default function task3a(req, res) {
  const params = req.params[0].replace(/\/+$/, '').split('/');
  let pcdata = req.pcdata;
  if (req.params[0] === '') {
    // FullData
  } else if (req.params[0].match(/^volumes\/?$/)) {
    // VolumesData
    pcdata = pcdata.hdd.reduce((p, item) => {
      const existVolume = p[item.volume] ? parseInt(p[item.volume], 10) : 0;
      p[item.volume] = `${existVolume + item.size}B`;
      return p;
    }, {});
  } else {
    // RequestData
    try {
      pcdata = params.reduce((p, item) => {
        if (typeof p[item] === 'undefined') {
          throw new Error();
        }
        if (typeof p === 'string') {
          throw new Error();
        }
        if ((p instanceof Array) && !item.match(/^\d+$/)) {
          throw new Error();
        }
        return p[item];
      }, req.pcdata);
    } catch (e) {
      res.status(404).send('Not Found');
      return;
    }
  }
  res.json(pcdata);
}
